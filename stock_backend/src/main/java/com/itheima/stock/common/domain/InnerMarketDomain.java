package com.itheima.stock.common.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/*
*
 * @author by itheima
 * @Date 2022/6/1
 * @Description 定义查询国内大盘数据的封装实体类
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InnerMarketDomain {
    /**
     * 大盘编码
     */
    private String code;
    /**
     * 大盘的名称
     */
    private String name;
    /**
     *大盘的开盘点
     */
    private BigDecimal openPoint;
    /**
     * 当前点
     */
    private BigDecimal curPoint;
    /**
     * 前收盘点
     */
    private BigDecimal preClosePoint;
    /**
     * 交易量
     */
    private Long tradeAmt;
    /**
     * 交易金额
     */
    private BigDecimal tradeVol;
    /**
     * 涨跌值
     */
    private BigDecimal upDown;
    /**
     * 涨幅
     */
    private BigDecimal rose;
    /**
     * 振幅
     */
    private BigDecimal amplitude;
    /**
     * 时间点，精确到分钟
     * 注解作用：springmvc底层会识别该注解，会自动将date转化成日期格式的字符串
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date curTime;

}
