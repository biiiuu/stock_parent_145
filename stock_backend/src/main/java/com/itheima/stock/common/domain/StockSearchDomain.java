package com.itheima.stock.common.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StockSearchDomain {
    /**
     * 股票编码
     */
    private String code;
    /**
     * 股票名称
     */
    private String name;


}
