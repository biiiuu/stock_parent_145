
package com.itheima.stock.common.domain;

        import com.fasterxml.jackson.annotation.JsonFormat;
        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        import java.io.Serializable;
        import java.math.BigDecimal;
        import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class externalMarketDomain implements Serializable {
    /**
     * 主键字段（无业务意义）
     */
    private String id;
    /**
     * 大盘编码
     */
    private String code;
    /**
     * 大盘的名称
     */
    private String name;

    /**
     * 当前点
     */
    private BigDecimal curPoint;



    /**
     * 涨跌值
     */
    private BigDecimal upDown;
    /**
     * 涨幅
     */
    private BigDecimal rose;

    /**
     * 时间点，精确到分钟
     * 注解作用：springmvc底层会识别该注解，会自动将date转化成日期格式的字符串
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date curTime;

}
