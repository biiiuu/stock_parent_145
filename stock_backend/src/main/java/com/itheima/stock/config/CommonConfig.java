package com.itheima.stock.config;

import com.itheima.stock.utils.IdWorker;
import com.itheima.stock.utils.ParserStockInfoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author by itheima
 * @Date 2022/5/30
 * @Description 定义公共的配置类
 */
@Configuration
public class CommonConfig {

    /**
     * 定义密码加密匹配器bean
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 配置分布式环境下全局唯一id的生成器
     * @return
     */
    @Bean
    public IdWorker idWorker(){
        /*
            参数1：机器id
            参数3：机房id
            也就是说，将来如果做集群部署的话，
            部署到不同机房下的机器，保证输入的参数不一致即可
         */
        IdWorker idWorker = new IdWorker(1L, 2L);
        return idWorker;
    }

    /**
     * 定义股票信息类转化bean
     * @param idWorker
     * @return
     */
    @Bean
    public ParserStockInfoUtil parserStockInfoUtil(@Autowired IdWorker idWorker){
        ParserStockInfoUtil parserStockInfoUtil = new ParserStockInfoUtil(idWorker);
        return parserStockInfoUtil;
    }

}
