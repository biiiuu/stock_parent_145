package com.itheima.stock.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author by itheima
 * @Date 2022/6/1
 * @Description
 */
@Data
@ConfigurationProperties(prefix = "stock")
//@Component
public class StockInfoConfig {
    /**
     * 国内大盘code集合
     */
    private List<String> inner;
    /**
     * 外盘code集合
     */
    private List<String> outer;
    /**
     * 股票涨跌区间定义
     */
    private List<String> upDownRange;
    /**
     * 大盘 板块 个股url的通用地址
     */
    private String marketUrl;
    /**
     * 板块的地址
     */
    private String blockUrl;
}
