package com.itheima.stock.constant;

/**
 * @author by itheima
 * @Date 2022/6/1
 * @Description
 */
public class ConstName {
    /**
     * 定义校验码的前缀
     */
    public static final String CHECK_PREFIX="CK:";

}
