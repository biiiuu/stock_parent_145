package com.itheima.stock.controller;

import com.itheima.stock.common.domain.*;
import com.itheima.stock.pojo.StockBusiness;
import com.itheima.stock.service.StockService;

import com.itheima.stock.vo.resp.PageResult;
import com.itheima.stock.vo.resp.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/quot")
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping("/stock/business/all")
    public List<StockBusiness> getAllBusiness(){
        return stockService.getAllStockBusiness();
    }


    /**
     * 获取最新国内A股大盘信息（上证+深证的大盘信息）
     * @return
     */
    @GetMapping("/index/all")
    public R<List<InnerMarketDomain>> getInnerMarketInfos(){
        return stockService. getInnerMarketInfos();
    }

    /**
     * 查询最新板块数据，按照交易金额降序取前10
     * @return
     */
    @GetMapping("/sector/all")
    public R<List<StockBlockDomain>> getStockBlockInfos(){
        return stockService.getStockBlockInfos();
    }
/**
 * day03-----查询涨幅前十
 * @return
 */
@GetMapping("/stock/increase")
    public R<List<StockUpdownDomain>> getStockIncreaseLimit(){
    return stockService.getStockIncreaseLimit();
}

    /**
     * 沪深两市个股行情列表查询 ,以时间顺序和涨幅分页查询
     * @param page 当前页
     * @param pageSize 每页大小
     * @return
     */
    @GetMapping("/stock/all")
    public R<PageResult<StockUpdownDomain>> stockPage(Integer page, Integer pageSize){
        return stockService.stockPage(page, pageSize);
    }
    /**
     * 功能描述：沪深两市涨跌停分时行情数据查询，查询T日每分钟的涨跌停数据（T：当前股票交易日）
     * 		查询每分钟的涨停和跌停的数据的同级；
     * 		如果不在股票的交易日内，那么就统计最近的股票交易下的数据
     * 	 map:
     * 	    upList:涨停数据统计
     * 	    downList:跌停数据统计
     * @return
     */
    @GetMapping("/stock/updown/count")
    public R<Map> upDownCount(){
        return stockService.upDownCount();
    }



    /**
     * 通过easyExcel导出指定页的股票涨幅信息
     * @param response 通过响应对象获取输出流，完成excel文件流对象的输入
     * @param page 当前页
     * @param pageSize 每页大小
     */
    @GetMapping("/stock/export")
    public void exportStockInfo(HttpServletResponse response,
                                @RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                                @RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize ){
        this.stockService.exportStockInfo(response,page,pageSize);
    }


    /**
     * day 04-----股票成交量对比
     */
    @GetMapping("/stock/tradevol")
    public R<Map> stockTradeVol4InnerMarker(){
        return stockService.stockTradeVol4InnerMarket();
    }

@GetMapping("/stock/updown")
    public R<Map> getStockUpDown(){
        return stockService.getStockUpDownSection();
}

/**
 *个股分时K线
 */
@GetMapping("/stock/screen/time-sharing")
public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(@RequestParam("code") String stockCode){
    return stockService.getStockScreenTimeSharing(stockCode);
}



    /**
     * 查询股票的日K线数据
     * @param stockCode 股票的编码
     * @return
     */
    @GetMapping("/stock/screen/dkline")
    public R<List<Stock4EvrDayDomain>> getStockScreenDkLine(@RequestParam("code") String stockCode){
        return stockService.getStockScreenDkLine(stockCode);
    }


    /**
     * DAY07----外盘指数展示
     */
    @GetMapping("/external/index")
    public R<List<externalMarketDomain>> externalIndexAll(){
        return stockService.getExternalMarketInfos();
    }

    /**
     * DAY07----模糊查询
     * stockCode---模糊查询条件
     */
    @GetMapping("/stock/search")
    @ResponseBody
    public R<List<StockSearchDomain>> getStockName(@RequestParam("searchStr") String stockCode){
        return stockService.searchStockByNum(stockCode);
    }
    /**
     * day07----个股描述
     */
    @GetMapping("/stock/describe")
    @ResponseBody
    public R<StockDescriptionDomain> getDescription(@RequestParam("code") String stcckCode){
        return stockService.searchDescriptionByCode(stcckCode);
    }


    /**
     * day07----个股周K------sql 待完成
     */
    @GetMapping("/stock/screen/weekkline")
    @ResponseBody
    public R<List<Stock4EvrWeekDomain>> getStockScreenWkLine(@RequestParam("code") String stockCode){
        return stockService.getStockScreenWkLine(stockCode);
    }

    /**
     * DAY08----最新分时行情数据
     */
    @GetMapping("/stock/screen/second/detail")
    @ResponseBody
    public R<Stock4MinuteDomain> getStockLatestInfo(@RequestParam("code" ) String stockCode){
        return stockService.getStockLatestInfo(stockCode);
    }

    /**
     * day08-----查询最新交易流水，按照交易时间降序取前10
     */

    @GetMapping("/stock/screen/second")
    @ResponseBody
    public R<StockLastestAmtDomain> getStockLastestAmt(@RequestParam("code") String stockCode){
        return stockService.getStockLastestAmt(stockCode);
    }
}
