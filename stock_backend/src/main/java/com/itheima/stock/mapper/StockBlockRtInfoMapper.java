package com.itheima.stock.mapper;

import com.itheima.stock.common.domain.StockBlockDomain;
import com.itheima.stock.pojo.StockBlockRtInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Entity com.itheima.stock.pojo.StockBlockRtInfo
 */
@Mapper
public interface StockBlockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBlockRtInfo record);

    int insertSelective(StockBlockRtInfo record);

    StockBlockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBlockRtInfo record);

    int updateByPrimaryKey(StockBlockRtInfo record);

    /**
     * 查询指定时间点下板块的数据，且按照交易金额，降序取前10
     * @param date
     * @return
     */
    List<StockBlockDomain> getStockBlockInfos(@Param("timPoint") Date date, @Param("limitNumber") Integer limitNumber);

}




