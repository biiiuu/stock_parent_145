package com.itheima.stock.mapper;

import com.itheima.stock.common.domain.StockDescriptionDomain;
import com.itheima.stock.pojo.StockBusiness;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.itheima.stock.pojo.StockBusiness
 */
@Mapper
public interface StockBusinessMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBusiness record);

    int insertSelective(StockBusiness record);

    StockBusiness selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBusiness record);

    int updateByPrimaryKey(StockBusiness record);


    List<StockBusiness> getAll();
    /**
     * 查询所有A股股票编码集合
     * @return
     */
    List<String> getAllStockCodes();

    /**
     * 根据股票编码查询股票描述
     * @param secCode
     * @return
     */
    StockDescriptionDomain searchDescriptionByCode(@Param("secCode")String secCode);
}




