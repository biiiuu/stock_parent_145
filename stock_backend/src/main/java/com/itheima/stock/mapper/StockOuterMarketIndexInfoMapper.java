package com.itheima.stock.mapper;

import com.itheima.stock.common.domain.InnerMarketDomain;
import com.itheima.stock.pojo.StockOuterMarketIndexInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Entity com.itheima.stock.pojo.StockOuterMarketIndexInfo
 */

@Mapper
public interface StockOuterMarketIndexInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockOuterMarketIndexInfo record);

    int insertSelective(StockOuterMarketIndexInfo record);

    StockOuterMarketIndexInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockOuterMarketIndexInfo record);

    int updateByPrimaryKey(StockOuterMarketIndexInfo record);

    /**
     * 根据大盘的编码和时间点查询数据
     * @param marketCodes 大盘编码集合
     * @param timePoint 时间点，精确到分钟
     * @return
     */
    List<InnerMarketDomain> getInnerMarketInfosByCodesAndTime(@Param("marketCodes") List<String> marketCodes,
                                                              @Param("timePoint") Date timePoint);

    /**
     *根据时间范围和指定的大盘id统计每分钟的交易量
     * @param tStartDate
     * @param tEndDate
     * @param innerCodes
     * @return
     */
    List<Map> getAmountInfoByRangeTime(@Param("startTime") Date tStartDate,
                                       @Param("endTime") Date tEndDate,
                                       @Param("marketCodes") List<String> innerCodes);
}




