package com.itheima.stock.mapper;

import com.itheima.stock.common.domain.*;
import com.itheima.stock.pojo.StockRtInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Entity com.itheima.stock.pojo.StockRtInfo
 */
@Mapper
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);

    /**
     * 根据指定时间点查询股票的涨幅信息
     * @param timePoint 时间点，精确到分钟
     * @return
     */
    List<StockUpdownDomain> getAllStockUpDownByTime(@Param("timePoint") Date timePoint);
    /**
     * 根据时间和涨幅降序排序全表查询
     * @return
     */
    List<StockUpdownDomain> stockAll();

    /**
     * 根据指定日期范围统计对应范围内每分钟的涨停或者跌停的数据
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param flag 标识: 1代表涨停 0：代表跌停
     * @return
     */
    List<Map> getStockUpDownCount(@Param("startTime") Date startTime,
                                  @Param("endTime") Date endTime,
                                  @Param("flag") int flag);
    /**
     * 统计指定时间点下股票在各个涨跌区间的数量
     * @param timePoint 时间点，精确到分钟
     * @return
     */
    List<Map> getStockUpDownSectionByTime(@Param("timePoint") Date timePoint);
    /**
     * 查询股票分时信息
     * @param startTime 起始时间
     * @param endTime 截止时间，保证与起始时间在同一天，且大于起始时间
     * @param stockCode 股票编码
     * @return
     */
    List<Stock4MinuteDomain> getStockSecondInfoByCodeAndTime(@Param("startTime") Date startTime,
                                                             @Param("endTime")  Date endTime,
                                                             @Param("stockCode") String stockCode);

    /**
     * 查询日K
     * @param startDate
     * @param endDate
     * @param stockCode
     * @return
     */
    List<Stock4EvrDayDomain> getStockEvyDayInfoByCodeAndTime(@Param("startTime") Date startDate,
                                                             @Param("endTime")   Date endDate,
                                                             @Param("stockCode") String stockCode);

    /**
     * 批量插入
     * @param list
     */
    int insertBatch(@Param("infos") List<StockRtInfo> list);

    /**
     * 根据数字模糊查询股票名称
     * @param searchStr 模糊查询数字
     * @return
     */
    List<StockSearchDomain> searchStockByNum(@Param("searchStr") String searchStr);

    /**
     * 查询股票周K线信息
     * @param startTime 起始时间
     * @param endTime 截止时间
     * @param stockCode 股票编码
     * @return
     */
    List<Stock4EvrWeekDomain> getStockEvyWeekInfoByCodeAndTime(@Param("startTime") Date startTime,
                                                               @Param("endTime")  Date endTime,
                                                               @Param("stockCode") String stockCode);

    /**
     * DAY08----最新分时行情数据
     */
    Stock4MinuteDomain getStockLatestInfo( @Param("stockCode") String stockCode);

    /**
     *  day08----个股实时交易流水查询
     */
    StockLastestAmtDomain getStockLastestAmt(@Param("stockCode") String stockCode);
}




