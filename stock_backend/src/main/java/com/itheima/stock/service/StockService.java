package com.itheima.stock.service;

import com.itheima.stock.common.domain.*;
import com.itheima.stock.pojo.StockBusiness;
import com.itheima.stock.vo.resp.PageResult;
import com.itheima.stock.vo.resp.R;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface StockService {
    List<StockBusiness> getAllStockBusiness();

    R<List<InnerMarketDomain>> getInnerMarketInfos();

    R<List<StockBlockDomain>> getStockBlockInfos();

    R<List<StockUpdownDomain>> getStockIncreaseLimit();
    /**
     * 沪深两市个股行情列表查询 ,以时间顺序和涨幅分页查询
     * @param page 当前页
     * @param pageSize 每页大小
     * @return
     */
    R<PageResult<StockUpdownDomain>> stockPage(Integer page, Integer pageSize);

    R<Map> upDownCount();


    /**
     * 通过easyExcel导出指定页的股票涨幅信息
     * @param response 通过响应对象获取输出流，完成excel文件流对象的输入
     * @param page 当前页
     * @param pageSize 每页大小
     */
    void exportStockInfo(HttpServletResponse response, Integer page, Integer pageSize);

    R<Map> stockTradeVol4InnerMarket();
    /**
     * 查询当前时间下股票的涨跌幅度区间统计功能
     * 如果当前日期不在有效时间内，则以最近的一个股票交易时间作为查询点
     * @return
     */
    R<Map> getStockUpDownSection();
    /**
     * 查询股票的分钟级流水数据--》分时线展示需要
     * @param stockCode 股票的编码
     * @return
     */
    R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode);

    /**
     * 查询股票的日K线数据
     * @param stockCode 股票的编码
     * @return
     */
    R<List<Stock4EvrDayDomain>> getStockScreenDkLine(String stockCode);


    R<List<externalMarketDomain>> getExternalMarketInfos();

    /**
     * 模糊查询股票名称
     * @param stockCode
     * @return
     */
    R<List<StockSearchDomain>> searchStockByNum(String stockCode);

    /**
     * 查询股票描述
     * @param stcckCode
     * @return
     */
    R<StockDescriptionDomain> searchDescriptionByCode(String stcckCode);
    /**
     * day07------查询股票的周K线数据
     * @param stockCode 股票的编码
     * @return
     */
    R<List<Stock4EvrWeekDomain>> getStockScreenWkLine(String stockCode);
    /**
     * DAY08----最新分时行情数据
     */
    R<Stock4MinuteDomain> getStockLatestInfo(String stockCode);

    /**
     *  day08----个股实时交易流水查询
     */
    R<StockLastestAmtDomain> getStockLastestAmt(String stockCode);



}
