package com.itheima.stock.task.job;

import com.itheima.stock.task.service.StockTimerTaskService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author by itheima
 * @Date 2022/6/7
 * @Description 定义定时采集股票数据信息的定时任务类
 */
@Component
public class StockServiceJob {

    @Autowired
    private StockTimerTaskService stockTimerTaskService;

    /**
     * 1、简单任务示例（Bean模式）
     * 采集国内大盘数据信息
     */
    @XxlJob("getStockMarketInfo")
    public void getStockMarketInfo(){
//       String time = DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
//     System.out.println("任务执行了,时间点为："+time);
        stockTimerTaskService.getInnerMarketInfos();
    }

    /**
     * 定时采集A股数据
     */
    @XxlJob("getStockAShare")
    public void getStockInfos(){
        stockTimerTaskService.getStockAShare();
    }

    /**
     * 板块定时任务
     */
    @XxlJob("getStockBlockInfoTask")
    public void getStockBlockInfoTask(){
        stockTimerTaskService.getStockSectorRtIndex();
    }






}
