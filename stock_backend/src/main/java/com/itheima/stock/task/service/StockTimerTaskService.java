package com.itheima.stock.task.service;

/**
 * @author by itheima
 * @Date 2022/6/5
 * @Description 定义定时采集股票数据的服务接口
 */
public interface  StockTimerTaskService {
    /**
     * 定义采集国内大盘数据的接口
     */
    void getInnerMarketInfos();

    /**
     * 采集A股实时数据方法
     */
    void getStockAShare();


    /**
     * 板块定时任务
     */
    void getStockSectorRtIndex();
}
