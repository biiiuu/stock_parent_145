package com.itheima.stock.task.service.impl;

import com.google.common.collect.Lists;
import com.itheima.stock.config.StockInfoConfig;
import com.itheima.stock.mapper.StockBusinessMapper;
import com.itheima.stock.mapper.StockMarketIndexInfoMapper;
import com.itheima.stock.mapper.StockRtInfoMapper;
import com.itheima.stock.pojo.StockMarketIndexInfo;
import com.itheima.stock.pojo.StockRtInfo;
import com.itheima.stock.task.service.StockTimerTaskService;
import com.itheima.stock.utils.DateTimeUtil;
import com.itheima.stock.utils.IdWorker;
import com.itheima.stock.utils.ParseType;
import com.itheima.stock.utils.ParserStockInfoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author by itheima
 * @Date 2022/6/5
 * @Description 定义股票数据采集服务实现
 */
@Service("stockTimerTaskService")
@Slf4j
public class StockTimerTaskServiceImpl implements StockTimerTaskService {

    /**
     * 注入httpClinet bean，调用远程股票api接口
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 注入股票参数配置类，方便获取股票相关的url地址信息
     */
    @Autowired
    private StockInfoConfig stockInfoConfig;

    /**
     * 注入id生成器，方便生成主键id值
     */
    @Autowired
    private IdWorker idWorker;

    @Autowired
    private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;

    @Autowired
    private StockBusinessMapper stockBusinessMapper;

    @Autowired
    private ParserStockInfoUtil parserStockInfoUtil;

    @Autowired
    private StockRtInfoMapper stockRtInfoMapper;



    /**
     * 采集国内大盘数据信息
     */
    @Override
    public void getInnerMarketInfos() {
        //1.组装url地址 eg:http://hq.sinajs.cn/list=sz000002,sh600015
        String innerMarketUrl=stockInfoConfig.getMarketUrl()+String.join(",",stockInfoConfig.getInner());
        //2.其它参数
        String referer="http://finance.sina.com.cn/realstock/company/sh000001/nc.shtml";
        String userAgent="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36";
        //3.组装http请求对象
        //3.1 组装请求头
        //设置请求头的请求参数类型
        HttpHeaders headers = new HttpHeaders();
        headers.add("Referer",referer);
        headers.add("User-Agent",userAgent);
        //3.2 组装请求对象
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        //4.发送请求
        String result = restTemplate.postForObject(innerMarketUrl, entity, String.class);
        System.out.println(result);
        if (result==null) {
            return;
        }
        /*
        var hq_str_sh000001="上证指数,3170.3055,3182.1566,3195.4580,3197.2818,3163.7555,0,0,361770632,413729263917,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2022-06-02,15:30:39,00,";
        var hq_str_sz399001="深证成指,11492.966,11551.272,11628.309,11637.534,11470.891,0.000,0.000,40615805083,477859353737.149,0,0.000,0,0.000,0,0.000,0,0.000,0,0.000,0,0.000,0,0.000,0,0.000,0,0.000,0,0.000,2022-06-02,15:00:03,00";
         */
        //定义公共正则表达式
        String reg="var hq_str_(.+)=\"(.+)\";";
        //编译表达式获取pattern对象
        Pattern pattern = Pattern.compile(reg);
        //匹配
        Matcher matcher = pattern.matcher(result);
        //收集解析的大盘数据实体对象
        List<StockMarketIndexInfo> list = new ArrayList<>();
        while (matcher.find()){
            String marketCode = matcher.group(1);
            //以逗号间隔的其它信息
            String otherInfos = matcher.group(2);
            //解析获取大盘基础数据
            String[] splitArr = otherInfos.split(",");
            //大盘名称
            String marketName=splitArr[0];
            //获取当前大盘的开盘点数
            BigDecimal openPoint=new BigDecimal(splitArr[1]);
            //前收盘点
            BigDecimal preClosePoint=new BigDecimal(splitArr[2]);
            //获取大盘的当前点数
            BigDecimal curPoint=new BigDecimal(splitArr[3]);
            //获取大盘最高点
            BigDecimal maxPoint=new BigDecimal(splitArr[4]);
            //获取大盘的最低点
            BigDecimal minPoint=new BigDecimal(splitArr[5]);
            //获取成交量
            Long tradeAmt=Long.valueOf(splitArr[8]);
            //获取成交金额
            BigDecimal tradeVol=new BigDecimal(splitArr[9]);
            //时间
            Date curTime = DateTimeUtil.getDateTimeWithoutSecond(splitArr[30] + " " + splitArr[31]).toDate();
            //组装大盘实体对象
            //组装entity对象
            StockMarketIndexInfo info = StockMarketIndexInfo.builder()
                    .id(idWorker.nextId() + "")
                    .marketCode(marketCode)
                    .marketName(marketName)
                    .curPoint(curPoint)
                    .openPoint(openPoint)
                    .preClosePoint(preClosePoint)
                    .maxPoint(maxPoint)
                    .minPoint(minPoint)
                    .tradeVolume(tradeVol)
                    .tradeAmount(tradeAmt)
                    .curTime(curTime)
                    .build();
            list.add(info);
        }
        log.info("1111111111当前采集的数据数量：{},内容：{}",list.size(),list);
        //批量插入
        int count= stockMarketIndexInfoMapper.insertBatch(list);
        log.info("当前插入了：{}条数据",count);
    }

    /**
     * 采集A股实时数据方法
     */
    @Override
    public void getStockAShare() {
        //1.获取所有A股股票的编码
        List<String> allCodes = stockBusinessMapper.getAllStockCodes();
        //2.添加股票前缀 sh sz
        List<String> prefixCodes = allCodes.stream().map(code -> {
            code = code.startsWith("6") ? "sh" + code : "sz" + code;
            return code;
        }).collect(Collectors.toList());
        //3.股票编码集合分片处理（每片大小15-一次性查询15支股票）
        //设置请求头的请求参数类型
        HttpHeaders headers = new HttpHeaders();
        headers.add("Referer","https://finance.sina.com.cn/stock/");
        headers.add("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        Lists.partition(prefixCodes, 15).stream().forEach(codeList->{
            //4.组装请求的地址
            String url=stockInfoConfig.getMarketUrl()+String.join(",",codeList);
            //5.请求获取数据
            String result = restTemplate.postForObject(url, entity, String.class);
            if (result==null) {
                return;
            }
            /*
            数据格式：
            var hq_str_sh601003="柳钢股份,4.260,4.250,4.330,4.350,4.220,4.330,4.340,11498037,49463802.000,152920,4.330,11900,4.320,23400,4.310,78000,4.300,29100,4.290,525100,4.340,502900,4.350,210600,4.360,138800,4.370,207600,4.380,2022-06-02,15:00:03,00,";
            var hq_str_sh601001="晋控煤业,15.000,15.130,14.410,15.000,14.240,14.410,14.420,35311591,512138157.000,22170,14.410,355008,14.400,22700,14.390,19900,14.380,16300,14.370,16900,14.420,6000,14.430,12600,14.440,20100,14.450,27800,14.460,2022-06-02,15:00:00,00,";
             */
            //6.解析数据，并封装对象
            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(result, ParseType.ASHARE);
            //7.批量保存
            int count = stockRtInfoMapper.insertBatch(list);
            log.info("插入记录数：{}条，内容：{}",count,list);
        });

    }

    @Override
    public void getStockSectorRtIndex() {

    }

}
