package com.itheima.stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootTest

public class TestAll {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Test
    public void testPwd(){
        String pwd="1234";
        String encode = passwordEncoder.encode(pwd);
/*        for (int i = 0; i < 10; i++) {
            String key=""+i;
            String encode1 = passwordEncoder.encode(key);
            System.out.println(i+" = " + encode1);
        }*/
        System.out.println(encode);
        boolean flag = passwordEncoder.matches(pwd,"$2a$10$WAWV.QEykot8sHQi6FqqDOAnevkluOZJqZJ5YPxSnVVWqvuhx88Ha");
        System.out.println(flag);
    }
}