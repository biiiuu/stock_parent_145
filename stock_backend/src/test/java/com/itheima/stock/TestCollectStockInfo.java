package com.itheima.stock;

import com.itheima.stock.task.service.StockTimerTaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author by itheima
 * @Date 2022/6/5
 * @Description
 */
@SpringBootTest
public class TestCollectStockInfo {

    @Autowired
    private StockTimerTaskService stockTimerTaskService;


    @Test
    public void testGetInnerInfo(){
        stockTimerTaskService.getInnerMarketInfos();
    }

    /**
     * @Description 测试采集a股数据
     */
    @Test
    public void testAshare(){
        stockTimerTaskService.getStockAShare();
    }

}
