package com.itheima.stock;

import com.itheima.stock.config.HttpClientConfig;
import com.itheima.stock.pojo.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootTest(classes = HttpClientConfig.class)
public class TestHttpClient {
    @Autowired
    private RestTemplate restTemplate;
    @Test
    public void testSimpleGet(){
        //url
        String url="http://localhost:6666/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        //请求头信息
        HttpHeaders headers = entity.getHeaders();
        //请求体信息
        String body = entity.getBody();

        int resCode = entity.getStatusCodeValue();
    }
    /**
     * @Description 测试get请求，获取json格式字符串
     */
    @Test
    public  void testGetPojo(){
        String url="http://localhost:6666/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        ResponseEntity<Account> entity = restTemplate.getForEntity(url, Account.class);
        Account accout = entity.getBody();
        System.out.println("accout = " + accout);
    }

    /**
     * @Description 测试get请求，获取json格式字符串
     */
    @Test
    public  void testGetPojo2(){
        String url="http://localhost:6666/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        Account accout = restTemplate.getForObject(url, Account.class);
        System.out.println("accout = " + accout);
    }

    /**
     * @Description 测试请求携带header数据
     */
    @Test
    public void testSendHeaders(){
        String url="http://localhost:6666/account/getHeader";
        //构建请求头对象
        HttpHeaders headers = new HttpHeaders();
        headers.add("userName","zhuxiaohu");
        //将请求头对象封装到请求对象中
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        //发送数据
        /*
            参数1：请求的url地址
            参数2：请求方式
            参数2：请求数据封装：包含请求头和请求体相关的数据
            参数4：响应数据类型
         */
        ResponseEntity<String> respEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        //获取响应数据
        String name = respEntity.getBody();
        System.out.println(name);
    }
    /**
     * @Description 测试模拟Form表单post请求发送数据
     */
    @Test
    public void testPostForm(){
        String url="http://localhost:6666/account/addAccount";
        //设置请求头，指定请求数据方式
        HttpHeaders headers = new HttpHeaders();
        //告知被调用方，请求方式是form表单提交，这样对方解析数据时，就会按照form表单的方式解析处理
        headers.add("Content-type","application/x-www-form-urlencoded");
        //组装模拟form表单提交数据，内部元素相当于form表单的input框
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("id","10");
        map.add("userName","itheima");
        map.add("address","shanghai");
        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<>(map, headers);
        //发送请求
        ResponseEntity<Account> resp = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Account.class);
        Account data = resp.getBody();
        System.out.println(data);
    }

    /**
     * post发送json数据:模拟发起ajax请求
     */
    @Test
    public void test05(){
        String url="http://localhost:6666/account/updateAccount";
        //设置请求头的请求参数类型
        HttpHeaders headers = new HttpHeaders();
        //告知被调用方，发送的数据格式的json格式，对方要以json的方式解析处理
        headers.add("Content-type","application/json; charset=utf-8");
        //组装json格式数据
//        HashMap<String, String> reqMap = new HashMap<>();
//        reqMap.put("id","1");
//        reqMap.put("userName","zhangsan");
//        reqMap.put("address","上海");
//        String jsonReqData = new Gson().toJson(reqMap);
        String jsonReq="{\"address\":\"上海\",\"id\":\"1\",\"userName\":\"zhangsan\"}";
        //构建请求对象
        HttpEntity<String> httpEntity = new HttpEntity<>(jsonReq, headers);
          /*
            发送数据
            参数1：请求url地址
            参数2：请求方式
            参数3：请求体对象，携带了请求头和请求体相关的参数
            参数4：响应数据类型
         */
        ResponseEntity<Account> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Account.class);
        //或者
        // Account account=restTemplate.postForObject(url,httpEntity,Account.class);
        Account body = responseEntity.getBody();
        System.out.println(body);
    }

    /**
     * 获取请求cookie值
     */
    @Test
    public void test06(){
        String url="http://localhost:6666/account/getCookie";
        ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
        //获取cookie
        List<String> cookies = result.getHeaders().get("Set-Cookie");
        //获取响应数据
        String resStr = result.getBody();
        System.out.println(resStr);
        System.out.println(cookies);
    }
}
