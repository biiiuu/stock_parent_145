package com.itheima.stock;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author by itheima
 * @Date 2021/12/30
 * @Description  测试redis集成
 */
@SpringBootTest
public class TestRedis {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test01(){
        //存入值
        redisTemplate.opsForValue().set("1111122222","8888");
        //获取值
        String checkCode =(String) redisTemplate.opsForValue().get("1111122222");
        System.out.println(checkCode);
    }
}