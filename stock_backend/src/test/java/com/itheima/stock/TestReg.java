package com.itheima.stock;



import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author by itheima
 * @Date 2022/6/5
 * @Description 测试java正则
 */
public class TestReg {

    @Test
    public void testRep2(){
        // 按指定模式在字符串查找
        String line = "This order was placed for QT3000! OK?";
        //定义表达式，表达式以（）分为三组，按照从左到右的顺序，一次是group1 gorup2 group3
        String pattern = "(\\D*)(\\d+)(.*)";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);
        //find()方法如果存在匹配的资源，则返回true，否则返回false
        if (m.find()) {
            //group0:表示所有表达式的公共匹配
            System.out.println("Found value: " + m.group(0) );
            System.out.println("Found value: " + m.group(1) );
            System.out.println("Found value: " + m.group(2) );
            System.out.println("Found value: " + m.group(3) );
        } else {
            System.out.println("NO MATCH");
        }
    }


}
